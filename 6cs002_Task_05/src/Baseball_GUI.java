import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

public class Baseball_GUI extends JFrame {
private JPanel contentPane;
	
	public Baseball_GUI() {
		setResizable(false);
		setTitle("2049389_6CS002_Task05");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(800, 800);
		setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel title = new JLabel("<html><h1><strong><i>Baseball Tournament</i></strong></h1><hr></html>");
		title.setBounds(210, 34, 45, 16);
		title.resize(210, 50);
		contentPane.add(title);
		
		JButton L1 = new JButton("Baseball L 01");
		L1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Baseball01.main(null);
			}
		});
		L1.setBounds(200, 80, 117, 29);
		L1.resize(200, 50);
		contentPane.add(L1);
		
		JButton L2 = new JButton("Baseball L 02");
		L2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Baseball02.main(null);
			}
		});
		L2.setBounds(200, 150, 117, 29);
		L2.resize(200, 50);
		contentPane.add(L2);
		
		JButton L3 = new JButton("Baseball L 03");
		L3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Baseball03.main(null);
			}
		});
		L3.setBounds(200, 220, 117, 29);
		L3.resize(200, 50);
		contentPane.add(L3);
		
		JButton L4 = new JButton("Baseball L 04");
		L4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Baseball04.main(null);
			}
		});
		L4.setBounds(200, 290, 117, 29);
		L4.resize(200, 50);
		contentPane.add(L4);
		
		JButton L5 = new JButton("Baseball L 05");
		L5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Baseball05.main(null);
			}
		});
		L5.setBounds(200, 360, 117, 29);
		L5.resize(200, 50);
		contentPane.add(L5);
		
		JButton exit = new JButton("Go Out");
		exit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		exit.setBounds(200, 440, 117, 29);
		exit.resize(200, 50);
		contentPane.add(exit);
	}
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Baseball_GUI frame = new Baseball_GUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
