/******************************************************************************
 * This class stores the details of a Premier Baseball club including the 
 * performance in the league. 

 ******************************************************************************/

public class Club implements Comparable<Club> {
  private int position;
  private String club;
  private int played;
  private int won;
  private int drawn;
  private int lost;
  private int RunsFor;
  private int RunsAgainst;
  private int RunsDifference;
  private int triesFor;
  private int triesAgainst;
  private int tryBonus;
  private int losingBonus;
  private int Runs;

  public Club(int position, String club, int played, int won, int drawn,
      int lost, int RunsFor, int RunsAgainst, int RunsDifference,
      int triesFor, int triesAgainst, int tryBonus, int losingBonus,
      int Runs) {
    this.position = position;
    this.club = club;
    this.played = played;
    this.won = won;
    this.drawn = drawn;
    this.lost = lost;
    this.RunsFor = RunsFor;
    this.RunsAgainst = RunsAgainst;
    this.RunsDifference = RunsDifference;
    this.triesFor = triesFor;
    this.triesAgainst = triesAgainst;
    this.tryBonus = tryBonus;
    this.losingBonus = losingBonus;
    this.Runs = Runs;
  }

  public String toString() {
    return String.format("%-3d%-20s%10d%10d%10d", position, club, RunsFor,
        RunsAgainst, Runs);
  }

  public int getPosition() {
    return position;
  }

  public void setPosition(int position) {
    this.position = position;
  }

  public String getClub() {
    return club;
  }

  public void setClub(String club) {
    this.club = club;
  }

  public int getPlayed() {
    return played;
  }

  public void setPlayed(int played) {
    this.played = played;
  }

  public int getWon() {
    return won;
  }

  public void setWon(int won) {
    this.won = won;
  }

  public int getDrawn() {
    return drawn;
  }

  public void setDrawn(int drawn) {
    this.drawn = drawn;
  }

  public int getLost() {
    return lost;
  }

  public void setLost(int lost) {
    this.lost = lost;
  }

  public int getRunsFor() {
    return RunsFor;
  }

  public void setRunsFor(int RunsFor) {
    this.RunsFor = RunsFor;
  }

  public int getRunsAgainst() {
    return RunsAgainst;
  }

  public void setRunsAgainst(int RunsAgainst) {
    this.RunsAgainst = RunsAgainst;
  }

  public int getRunsDifference() {
    return RunsDifference;
  }

  public void setRunsDifference(int RunsDifference) {
    this.RunsDifference = RunsDifference;
  }

  public int getTriesFor() {
    return triesFor;
  }

  public void setTriesFor(int triesFor) {
    this.triesFor = triesFor;
  }

  public int getTriesAgainst() {
    return triesAgainst;
  }

  public void setTriesAgainst(int triesAgainst) {
    this.triesAgainst = triesAgainst;
  }

  public int getTryBonus() {
    return tryBonus;
  }

  public void setTryBonus(int tryBonus) {
    this.tryBonus = tryBonus;
  }

  public int getLosingBonus() {
    return losingBonus;
  }

  public void setLosingBonus(int losingBonus) {
    this.losingBonus = losingBonus;
  }

  public int getRuns() {
    return Runs;
  }

  public void setRuns(int Runs) {
    this.Runs = Runs;
  }
  
  public int compareTo(Club c) {
    return ((Integer) RunsFor).compareTo(c.RunsFor);
  }
}
