import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class Baseball01 {
  public static void main(String[] args) {
    List<Club> table = Arrays.asList(
        new Club(1, "Yankees", 2, 6, 1, 5, 6, 9, 9, 7, 8,
            8, 2, 7),
        new Club(2, "Rangers", 2, 1, 0, 6, 5, 4, 2, 7, 4, 9, 2, 5),
        new Club(3, "Cardinals", 2, 5, 1, 6, 5, 4, 3, 7, 3, 4,
            2, 8),
        new Club(4, "Braves", 2, 4, 1, 7, 6, 8, 4, 7, 4, 5, 5, 8),
        new Club(5, "Rockies", 3, 1, 0, 8, 6, 7, 2, 7, 6, 4, 7,
            8),
        new Club(6, "Astros", 2, 1, 2, 5, 6, 7, 4, 7, 5, 9, 4, 6),
        new Club(7, "Rays", 2, 1, 0, 1, 6, 8, 5, 6, 5, 6, 4,
            5),
        new Club(8, "Marlins", 2, 1, 0, 2, 4, 5, 7, 5, 5, 4, 5,
            9),
        new Club(9, "Red Sox", 2, 9, 1, 2, 5, 7, 9, 3, 6, 4, 6,
            8),
        new Club(10, "Diamond Backs", 2, 7, 1, 4, 4, 7, 6, 6, 7, 4, 6,
            4),
        new Club(11, "Brewers", 2, 5, 1, 6, 7, 5, 7, 7, 6,
            4, 8, 4),
        new Club(12, "Dodgers", 2, 3, 3, 2, 3, 7, 8, 9, 7, 1,
            0, 1));

     table.forEach(x -> System.out.println(x));
     try {
			FileWriter writer = new FileWriter("OutputFile01.txt");
			
			table.forEach(x -> {
			try {
				writer.write(x + "\n");
			} catch (IOException e) {
				e.printStackTrace();
			}
		});
			writer.close();
			System.out.println("\nSuccessfully wrote to the file OutputFile01.txt.");
	    } catch (IOException e) {
	    	System.out.println("Error occurred..!!!!");
	    	e.printStackTrace();
	    }

  }

}
